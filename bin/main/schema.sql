CREATE TABLE users
(
	user_id INT NOT NULL AUTO_INCREMENT,
	user_name VARCHAR NOT NULL,
	password VARCHAR NOT NULL,
	birthday DATETIME NOT NULL default current_timestamp,
	sex int NOT NULL,
	alive_flag boolean default true,
	admin_flag boolean default true,
	created_date DATETIME NOT NULL default current_timestamp,
	updated_date DATETIME NOT NULL default current_timestamp,
	PRIMARY KEY(user_id)
);

--CREATE TABLE survey
--(
--   id INT NOT NULL AUTO_INCREMENT,
--   age INT NOT NULL,
--   satisfaction INT NOT NULL,
--   comment VARCHAR(100),
--   created DATETIME NOT NULL,
--   PRIMARY KEY(id)
--);

package fscomics.service;

import java.util.List;
import java.util.Optional;

import fscomics.entity.User;

public interface UserService {

	List<User> getAll();

//	List<User> findAll();
//
	Optional<User> getUser(int userId);

	void insert(User user);

	void update(User user);

//	void deleteById(int id);
}

package fscomics.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fscomics.entity.User;
import fscomics.repository.UserDao;

@Service
public class UserServiceImpl implements UserService {


	private final UserDao dao;

	@Autowired
	public UserServiceImpl(UserDao dao) {
		this.dao = dao;
	}

	@Override
	public List<User> getAll() {
		return dao.getAll();
	}

//	@Override
//	public List<Task> findAll() {
//		return dao.findAll();
//	}
//
	@Override
	public Optional<User> getUser(int userId) {

		//Optional<Task>一件を取得 idが無ければ例外発生　
		try {
			return dao.findById(userId);
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException("指定されたタスクが存在しません");
		}
	}

	@Override
	public void insert(User user) {
		dao.insert(user);
	}

	@Override
	public void update(User user) {

		//Taskを更新　idが無ければ例外発生
		if (dao.update(user) == 0) {
			throw new UserNotFoundException("更新するタスクが存在しません");
		}
	}

//	@Override
//	public void deleteById(int id) {
//
//		//Taskを更新 idがなければ例外発生
//		if (dao.deleteById(id) == 0) {
//			throw new TaskNotFoundException("削除するタスクが存在しません");
//		}
//	}
}


package fscomics.controller;

import java.time.LocalDateTime;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class UserForm {

	//Javaのデフォルト値が入るようになる
	public UserForm() {}

	public UserForm(int userId, String userName, String password, LocalDateTime birthday,
			int sex, boolean aliveFlag, boolean adminFlag) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.birthday = birthday;
		this.sex = sex;
		this.aliveFlag = aliveFlag;
		this.adminFlag = adminFlag;
	}

    @Digits(integer = 1, fraction = 0)
    private int userId;

    @NotNull (message = "名前を入力してください。")
    @Size(min = 1, max = 20, message="20文字以内で入力してください。")
    private String userName;

    @NotNull (message = "パスワードを入力してください。")
    private String password;

    @NotNull (message = "誕生日を設定してください。")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    //@Future (message = "期限が過去に設定されています。")
    private LocalDateTime birthday;

    @Digits(integer = 1, fraction = 0)
    private int sex;

    public boolean aliveFlag;

    public boolean adminFlag;



	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDateTime birthday) {
		this.birthday = birthday;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public boolean isAliveFlag() {
		return aliveFlag;
	}

	public void setAliveFlag(boolean aliveFlag) {
		this.aliveFlag = aliveFlag;
	}

	public boolean isAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(boolean adminFlag) {
		this.adminFlag = adminFlag;
	}


}
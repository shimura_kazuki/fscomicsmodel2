package fscomics.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fscomics.entity.User;
import fscomics.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;
//	private final UserService userService;
//
//    @Autowired
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }

	@GetMapping("/signup")
	public String form(UserForm userForm, Model model, @ModelAttribute("complete") String complete) {
		model.addAttribute("title", "User Form");
		return "user/signup";
	}

	@PostMapping("/signup")
	public String formGoBack(UserForm userForm, Model model) {
		model.addAttribute("title", "UserForm");
		return "user/signup";
	}


	@PostMapping("/confirm")
	public String confirm(@Validated UserForm userForm, BindingResult result, Model model) {

		if(result.hasErrors()) {
			model.addAttribute("title", "Inquiry Form");
			return "user/signup";
		}
		model.addAttribute("title", "Confirm Page");
		return "user/confirm";
	}

	@PostMapping("/complete")
	public String complete(@Validated UserForm userForm,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			model.addAttribute("title", "UserForm");
			return "user/signup";
		}

		User user = new User();
		user.setUserName(userForm.getUserName());
		user.setPassword(userForm.getPassword());
		user.setBirthday(userForm.getBirthday());
		user.setSex(userForm.getSex());

		userService.insert(user);

		redirectAttributes.addFlashAttribute("complete", "Registered!");
		return "redirect:/";
	}

	@GetMapping("/list")
	public String userList(Model model) {
		List<User> list = userService.getAll();
		model.addAttribute("userList", list);
		model.addAttribute("title", "User Index");
		return "user/list";
	}


	@GetMapping("/details/{userId}")
	public String userDetails(Model model, @PathVariable int userId, UserForm userForm) {

		//Taskを取得(Optionalでラップ)
    	Optional<User> userOpt = userService.getUser(userId);
    	//TaskFormへの詰め直し
    	Optional<UserForm> userFormOpt = userOpt.map(t -> makeUserForm(t));
    	//TaskFormがnullでなければ中身を取り出し
    	if (userFormOpt.isPresent()) {
    		userForm = userFormOpt.get();
    	}

		model.addAttribute("userForm", userForm);
		model.addAttribute("title", "User Index");
		return "user/details";
	}

	@PostMapping("/update/{userId}")
    public String update(
    		@Valid @ModelAttribute UserForm userForm,
	    	BindingResult result,
	    	@PathVariable int userId,
	    	Model model,
	    	RedirectAttributes redirectAttributes) {

    	//TaskFormのデータをTaskに格納
    	User user = makeUser(userForm, userId);

//    	エラーが発生する（日付の型で）
//    	if (result.hasErrors()) {
//			model.addAttribute("userForm", userForm);
//			model.addAttribute("title", "タスク一覧");
//			return "user/details";
//		}

    	//更新処理、フラッシュスコープの使用、リダイレクト（個々の編集ページ）
    	userService.update(user);
    	redirectAttributes.addFlashAttribute("complete", "更新が完了しました");
        return "redirect:/user/details/" + userId;
    }

	private User makeUser(UserForm userForm, int userId) {
        User user = new User();
        if(userId != 0) {
        	user.setUserId(userId);
        }
        user.setUserName(userForm.getUserName());
        user.setPassword(userForm.getPassword());
        user.setSex(userForm.getSex());
		user.setBirthday(userForm.getBirthday());
        return user;
    }

	//表示用にUserFormに入れ直す
	private UserForm makeUserForm(User user) {

        UserForm userForm = new UserForm();

        userForm.setUserId(user.getUserId());
        userForm.setUserName(user.getUserName());
        userForm.setPassword(user.getPassword());
        userForm.setBirthday(user.getBirthday());
        userForm.setSex(user.getSex());

        return userForm;
    }

}

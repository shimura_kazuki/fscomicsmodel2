package fscomics.repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import fscomics.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public UserDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<User> getAll() {

		String sql = "select * from users";

		List<Map<String, Object>> resultList = jdbcTemplate.queryForList(sql);

		List<User> userlist = new ArrayList<User>();
		for(Map<String, Object> result : resultList) {
			User user = new User();
			user.setUserId((int)result.get("user_id"));
			user.setUserName((String)result.get("user_name"));
			user.setPassword((String)result.get("password"));
			user.setBirthday(((Timestamp)result.get("birthday")).toLocalDateTime());
			user.setSex((int)result.get("sex"));
			user.setAliveFlag((boolean)result.get("alive_flag"));
			user.setAdminFlag((boolean)result.get("admin_flag"));
			user.setCreatedDate(((Timestamp)result.get("created_date")).toLocalDateTime());
			user.setUpdatedDate(((Timestamp)result.get("created_date")).toLocalDateTime());
			userlist.add(user);
		}

		return userlist;
	}

//	@Override
//	public List<User> findAll() {
//
//		String sql = "SELECT task.id, user_id, type_id, title, detail, deadline, "
//				+ "type, comment FROM task "
//				+ "INNER JOIN task_type ON task.type_id = task_type.id";
//
//		//削除してください
//
//		//タスク一覧をMapのListで取得
//		List<Map<String, Object>> resultList = jdbcTemplate.queryForList(sql);
//
//		//return用の空のListを用意
//		List<User> list = new ArrayList<User>();
//
//		//二つのテーブルのデータをTaskにまとめる
//		for(Map<String, Object> result : resultList) {
//
//			User user = new User();
//			user.setId((int)result.get("id"));
//			user.setUserId((int)result.get("user_id"));
//			user.setTypeId((int)result.get("type_id"));
//			user.setTitle((String)result.get("title"));
//			user.setDetail((String)result.get("detail"));
//			user.setDeadline(((Timestamp) result.get("deadline")).toLocalDateTime());
//
//			UserType type = new UserType();
//			type.setId((int)result.get("type_id"));
//			type.setType((String)result.get("type"));
//			type.setComment((String)result.get("comment"));
//
//			//TaskにTaskTypeをセット
//			user.setUserType(type);
//
//			list.add(user);
//		}
//		return list;
//	}

	@Override
	public Optional<User> findById(int userId) {
		String sql = "SELECT * FROM users WHERE users.user_id = ?";

		//タスクを一件取得
		Map<String, Object> result = jdbcTemplate.queryForMap(sql, userId);

		User user = new User();
		user.setUserId((int)result.get("user_id"));
		user.setUserName((String)result.get("user_name"));
		user.setPassword((String)result.get("password"));
		user.setBirthday(((Timestamp)result.get("birthday")).toLocalDateTime());
		user.setSex((int)result.get("sex"));
		user.setAliveFlag((boolean)result.get("alive_flag"));
		user.setAdminFlag((boolean)result.get("admin_flag"));
		user.setCreatedDate(((Timestamp)result.get("created_date")).toLocalDateTime());
		user.setUpdatedDate(((Timestamp)result.get("created_date")).toLocalDateTime());

		//taskをOptionalでラップする
		Optional<User> userOpt = Optional.ofNullable(user);

		return userOpt;
	}

	@Override
	public void insert(User user) {
		jdbcTemplate.update("INSERT INTO users(user_name, password, birthday, sex, "
				+ "alive_flag, admin_flag"
				+ ") VALUES("
				+ "?, ?, ?, ?, ?, ?)",
				 user.getUserName(), user.getPassword(), user.getBirthday(), user.getSex(),
				 "true", "true");
	}

	@Override
	public int update(User user) {
		return jdbcTemplate.update("UPDATE users SET user_id = ?, user_name = ?, password = ?,"
				+ "sex = ? WHERE user_id = ?",
				user.getUserId(), user.getUserName(), user.getPassword(), user.getSex(), user.getUserId() );

	}

//	@Override
//	public int deleteById(int userId) {
//		return jdbcTemplate.update("DELETE FROM users WHERE user_id = ?", userId);
//	}

}

package fscomics.repository;

import java.util.List;
import java.util.Optional;

import fscomics.entity.User;

public interface UserDao {

	List<User> getAll();

//	List<User> findAll();
//
	Optional<User> findById(int userId);

	void insert(User user);

	int update(User user);

//	int deleteById(int id);

}
